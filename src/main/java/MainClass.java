import dao.StudentHibernateDao;
import model.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {
    public static void main(String[] args) {
        System.out.println("hello world !");
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
//        StudentJdbcDao studentJdbcDao = context.getBean("studentJdbcDao",StudentJdbcDao.class);

        // Bean life cycle
//        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(BeanConfig.class);
//        FooService fooService = ctx.getBean(FooService.class);
//        fooService.doStuff();
        //ctx.close();


        StudentHibernateDao studentHibernateDao = context.getBean("studentHibernateDao",StudentHibernateDao.class);
        Student student = new Student("vuongnh","Beclin");
        studentHibernateDao.save(student);
    }
}
