package testbean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class FooService {

    public void init(){
        System.out.println("bean just init 1");
    }

    public void doStuff(){
        System.out.println(" FooService running !");
    }
    public void destroy(){
        System.out.println(" bean just destroy 1");
    }

    @PostConstruct
    public void postConstruct(){
        System.out.println(" post construct run !");
    }

    @PreDestroy
    public void preDestroy(){
        System.out.println(" pre destroy run !");
    }
}
